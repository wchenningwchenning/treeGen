#include "rendering/model_renderer.hpp"
#include "tree/grove.hpp"
#include "tree/mesh.hpp"
#include "tree/species.hpp"

#include <ctime>
#include <glm/vec3.hpp>
#include <iostream>
#include <thread>

void grow(Grove *grove, bool *dirty) {
  for (int i = 1; i <= 30; i++) {
    std::cout << "Grown for " << i << " years." << std::endl;
    grove->tick_years(1);
    *dirty = true;
  }
  grove->print_stats();
}

int main() {
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  GLFWwindow *window =
      glfwCreateWindow(1440, 900, "William's Tree Emporium", nullptr, nullptr);

  ModelRenderer renderer(window);

  glfwSetWindowUserPointer(window, &renderer);
  glfwSetWindowSizeCallback(window, ModelRenderer::resizeCallback);

  Grove grove(/*x_length=*/60, /*y_length=*/60, /*light_volume_height=*/60);
  grove.add_tree(glm::vec3(30, 30, 0), Species());

  bool dirty = false;
  std::thread t(grow, &grove, &dirty);

  int f = 0;
  while (!glfwWindowShouldClose(window)) {
    f = f + 1 % 100000;
    float t = f / 100.0;
    //   std::cout << t << std::endl;
    glfwPollEvents();
    float distance = 70;
    renderer.pointCamera(
        glm::vec3(30 + distance, 30 + distance, distance * 3 / 2),
        glm::vec3(-1, -1, -1), t);
    //    std::cos(t), 0));
    if (dirty) {
      Mesh grove_mesh = Mesh::from_grove(grove);
      renderer.bufferMesh(grove_mesh);
      dirty = false;
    }
    renderer.redraw();
  }

  return 0;
}
