#ifndef DOUBLE_PLANE_HPP
#define DOUBLE_PLANE_HPP

#include "vertex.hpp"

#include <vector>

namespace DoublePlane {
const std::vector<Vertex> vertices = {
    {{-.5, -.5, .0}, {0, 1, 0}},  {{.5, -.5, .0}, {0, 1, 0}},
    {{.5, .5, .0}, {0, 1, 0}},    {{-.5, .5, .0}, {0, 1, 0}},

    {{-.5, -.5, -.3}, {1, 0, 0}}, {{.5, -.5, -.3}, {1, 0, 0}},
    {{.5, .5, -.3}, {1, 0, 0}},   {{-.5, .5, -.3}, {1, 0, 0}}};

const std::vector<uint32_t> indices = {0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4};
} // namespace DoublePlane

#endif /* DOUBLE_PLANE_HPP */
