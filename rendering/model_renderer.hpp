#ifndef MODEL_RENDERER_HPP
#define MODEL_RENDERER_HPP

#include "tree/mesh.hpp"
#include "vulkan_renderer.hpp"

#include <GLFW/glfw3.h>

// Code to render generated trees
class ModelRenderer {
  Renderer renderer;
  double fieldOfView = M_PI / 4;

public:
  ModelRenderer(GLFWwindow *window) : renderer(window) {}

  void bufferMesh(const Mesh &mesh) {
    renderer.bufferVertices(mesh.vertices);
    renderer.bufferIndices(mesh.indices);
    renderer.rerecordCommandBuffers();
  }

  void redraw() { renderer.drawFrame(); }

  void pointCamera(glm::vec3 position, glm::vec3 direction, float rotation) {
    renderer.bufferModelViewProjection(position, direction, fieldOfView,
                                       rotation);
  }

  void bufferModelUniform(const UniformBufferObject &ubo) {
    renderer.bufferModelUniform(ubo);
  }

  void onResize(int width, int height) { renderer.onResize(width, height); }

  static void resizeCallback(GLFWwindow *window, int width, int height) {
    ModelRenderer *renderer =
        reinterpret_cast<ModelRenderer *>(glfwGetWindowUserPointer(window));
    renderer->onResize(width, height);
  }
};

#endif /* MODEL_RENDERER_HPP */
