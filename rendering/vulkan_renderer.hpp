#ifndef VULKAN_RENDERER_HPP
#define VULKAN_RENDERER_HPP

// A "simple" model renderer

#include "vertex.hpp"

#define GLFW_INCLUDE_VULKAN
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <GLFW/glfw3.h>
#include <cstring>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>

struct UniformBufferObject {
  glm::mat4 model;
  glm::mat4 view;
  glm::mat4 proj;
};

class Renderer {
public:
  // Prevent unnecessary work.
  void onResize(int width, int height) {
    if (width != 0 && height != 0) {
      recreateSwapChain();
    }
  }

  void bufferModelUniform(const UniformBufferObject &ubo) {
    void *data;
    vkMapMemory(device, uniformBufferMemory, 0, sizeof(ubo), 0, &data);
    memcpy(data, &ubo, sizeof(ubo));
    vkUnmapMemory(device, uniformBufferMemory);
  }

  void bufferModelViewProjection(glm::vec3 cameraPosition,
                                 glm::vec3 cameraDirection, float fieldOfView,
                                 float model_rotation) {
    UniformBufferObject ubo{
        glm::translate(
            glm::rotate(glm::translate(glm::mat4(1.0f), glm::vec3(30, 30, 0)),
                        model_rotation, glm::vec3(0, 0, 1)),
            glm::vec3(-30, -30, 0)),
        glm::lookAt(cameraPosition, cameraPosition + cameraDirection,
                    glm::vec3(0, 0, 1)),
        glm::perspective(fieldOfView, (float)aspectRatio(), .1f, 500.0f)};
    ubo.proj[1][1] *= -1;
    bufferModelUniform(ubo);
  }

  void bufferVertices(std::vector<Vertex> vertices);

  void bufferIndices(std::vector<uint32_t> indices);

  void rerecordCommandBuffers();

  double aspectRatio() {
    return swapChainExtent.width / (double)swapChainExtent.height;
  }

  void drawFrame();

  Renderer(GLFWwindow *window) : window(window) { initVulkan(); }

  ~Renderer() { cleanup(); }

private:
  static const int WIDTH = 1440;
  static const int HEIGHT = 900;

  GLFWwindow *window;
  VkInstance instance;
  VkDebugReportCallbackEXT callback;

  VkSurfaceKHR surface;
  VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
  VkDevice device;

  VkQueue graphicsQueue;
  VkQueue presentQueue;

  VkSwapchainKHR swapChain;
  std::vector<VkImage> swapChainImages;
  std::vector<VkImageView> swapChainImageViews;
  VkExtent2D swapChainExtent;
  VkFormat swapChainImageFormat;

  VkImage depthImage;
  VkDeviceMemory depthImageMemory;
  VkImageView depthImageView;

  VkRenderPass renderPass;
  VkDescriptorSetLayout descriptorSetLayout;
  VkPipelineLayout pipelineLayout;
  VkPipeline graphicsPipeline;
  std::vector<VkFramebuffer> swapChainFramebuffers;

  VkCommandPool commandPool;
  std::vector<VkCommandBuffer> commandBuffers;

  VkBuffer vertexBuffer = VK_NULL_HANDLE;
  VkDeviceMemory vertexBufferMemory = VK_NULL_HANDLE;

  VkBuffer indexBuffer = VK_NULL_HANDLE;
  VkDeviceMemory indexBufferMemory = VK_NULL_HANDLE;
  int numIndices = 0;

  VkBuffer uniformBuffer = VK_NULL_HANDLE;
  VkDeviceMemory uniformBufferMemory = VK_NULL_HANDLE;

  VkDescriptorPool descriptorPool;
  VkDescriptorSet descriptorSet;

  VkSemaphore imageAvailableSemaphore;
  VkSemaphore renderFinishedSemaphore;

  struct QueueFamilyIndices {
    int graphicsFamily = -1;
    int presentFamily = -1;
    bool isComplete() { return graphicsFamily >= 0 && presentFamily >= 0; }
  };

  QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);

  struct SwapChainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;
  };

  SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);
  void createInstance();
  bool checkValidationLayerSupport();
  std::vector<const char *> getRequiredExtensions();
  void initVulkan();
  uint32_t findMemoryType(uint32_t typeFilter,
                          VkMemoryPropertyFlags properties);
  VkFormat findSupportedFormat(const std::vector<VkFormat> &candidates,
                               VkImageTiling tiling,
                               VkFormatFeatureFlags features);
  bool hasStencilComponent(VkFormat format);
  VkFormat findDepthFormat();
  VkImageView createImageView(VkImage image, VkFormat format,
                              VkImageAspectFlags aspectFlags);
  void createImage(uint32_t width, uint32_t height, VkFormat format,
                   VkImageTiling tiling, VkImageUsageFlags usage,
                   VkMemoryPropertyFlags properties, VkImage &image,
                   VkDeviceMemory &imageMemory);
  VkCommandBuffer beginSingleTimeCommands();
  void endSingleTimeCommands(VkCommandBuffer commandBuffer);
  void transitionImageLayout(VkImage image, VkFormat format,
                             VkImageLayout oldLayout, VkImageLayout newLayout);
  void createDepthResources();
  void createDescriptorSet();
  void createDescriptorPool();
  template <typename T>
  void bufferArray(std::vector<T> vec, VkBufferUsageFlags usage,
                   VkBuffer *buffer, VkDeviceMemory *bufferMemory);
  void createUniformBuffer();
  void createDescriptorSetLayout();
  void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage,
                    VkMemoryPropertyFlags properties, VkBuffer *buffer,
                    VkDeviceMemory *bufferMemory);
  void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
  void createSemaphores();

  void createCommandBuffers();
  void recordCommandBuffers();

  void createCommandPool();
  void createFramebuffers();
  void createRenderPass();
  void createGraphicsPipeline();
  VkShaderModule createShaderModule(const std::vector<char> &code);
  void createImageViews();
  void createSurface();
  void createLogicalDevice();
  void cleanupSwapChain();
  void recreateSwapChain();
  void createSwapChain();
  bool isDeviceSuitable(VkPhysicalDevice device);
  VkSurfaceFormatKHR chooseSwapSurfaceFormat(
      const std::vector<VkSurfaceFormatKHR> &availableFormats);
  VkPresentModeKHR chooseSwapPresentMode(
      const std::vector<VkPresentModeKHR> /*availablePresentModes*/);
  VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities);
  bool checkDeviceExtensionSupport(VkPhysicalDevice device);
  void pickPhysicalDevice();
  void setupDebugCallback();
  void updateUniformBuffer();
  void cleanup();
};

#endif /*VULKAN_RENDERER_HPP*/
