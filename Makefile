###############################################################

# Builds TreeGen
CXX = g++
TEST_DIR = ./tests
BUILD_DIR = ./build

# Points to the root of Google Test, relative to where this file is.
# Remember to tweak this if you move this file, or if you want to use
# a copy of Google Test at a different location.
GTEST_DIR = ./external/googletest/googletest

# Points to the root of Google Mock, relative to where this file is.
# Remember to tweak this if you move this file.
GMOCK_DIR = ./external/googletest/googlemock

# Flags passed to the C++ compiler.
CXXFLAGS += -Wall -Wextra -pedantic -std=c++14 -I.
CXXFLAGS += -lvulkan -lglfw -pthread
CXXFLAGS += -DGLM_ENABLE_EXPERIMENTAL

# Tells compiler to generate and use build dependencies?
CPPFLAGS += -MMD -MP

SYSTEM_INCLUDE_FOLDERS = $(GTEST_DIR) $(GMOCK_DIR)
CXXFLAGS += $(SYTEM_INCLUDE_FOLDERS:%=-isystem %)

SRC_FOLDERS = tree math rendering
CPP_FILES = $(wildcard $(addsuffix /*.cpp,$(SRC_FOLDERS)))

TEST_SRCS = $(filter %test.cpp, $(CPP_FILES))
TEST_TARGS = $(addprefix $(TEST_DIR)/,$(subst .cpp,,$(TEST_SRCS)))

EXES = demo.cpp
NON_EXE_SRCS = $(filter-out %test.cpp $(EXES), $(CPP_FILES))
OBJS = $(addprefix $(BUILD_DIR)/,$(NON_EXE_SRCS:.cpp=.o))

debug: CXXFLAGS += -g3 -DDEBUG
debug: all

profile: CXXFLAGS += -DNDEBUG -g -O3
profile: all

release: CXXFLAGS += -O3 -flto -DNDEBUG
release: all

test_debug: CXXFLAGS += -g3 -DDEBUG
test_debug: test

all: demo

make_build_dir=@mkdir -p $(@D)
make_test_dir=@mkdir -p $(@D)

demo: demo.o $(OBJS)
	$(CXX) $(CXXFLAGS) -o demo $^

$(BUILD_DIR)/%.o: %.cpp %.hpp
	$(make_build_dir)
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(TEST_DIR)/%_test.o: %_test.cpp
	$(make_test_dir)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

%_test: %_test.o gmock_main.a $(OBJS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -lpthread $^ -o $@

test: $(TEST_TARGS)

clean:
	rm -f $(TEST_TARGS) $(OBJS) *.d
	rm -f demo demo.o demo.d
	rm -rf $(BUILD_DIR)
	rm -rf $(TEST_DIR)

###############################################################

# Builds GMOCK
test: CPPFLAGS += -isystem $(GTEST_DIR)/include -isystem $(GMOCK_DIR)/include

# All Google Test headers.  Usually you shouldn't change this
# definition.
GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h

# All Google Mock headers. Note that all Google Test headers are
# included here too, as they are #included by Google Mock headers.
# Usually you shouldn't change this definition.
GMOCK_HEADERS = $(GMOCK_DIR)/include/gmock/*.h \
                $(GMOCK_DIR)/include/gmock/internal/*.h \
                $(GTEST_HEADERS)

# Usually you shouldn't tweak such internal variables, indicated by a
# trailing _.
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)
GMOCK_SRCS_ = $(GMOCK_DIR)/src/*.cc $(GMOCK_HEADERS)



# For simplicity and to avoid depending on implementation details of
# Google Mock and Google Test, the dependencies specified below are
# conservative and not optimized.  This is fine as Google Mock and
# Google Test compile fast and for ordinary users their source rarely
# changes.
gtest-all.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GTEST_DIR)/src/gtest-all.cc

gmock-all.o : $(GMOCK_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GMOCK_DIR)/src/gmock-all.cc

gmock_main.o : $(GMOCK_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GMOCK_DIR)/src/gmock_main.cc

gmock.a : gmock-all.o gtest-all.o
	$(AR) $(ARFLAGS) $@ $^

gmock_main.a : gmock-all.o gtest-all.o gmock_main.o
	$(AR) $(ARFLAGS) $@ $^

-include $(CPP_FILES:%.cpp=%.d)
