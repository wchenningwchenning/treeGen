#include "vector_matchers.hpp"
#include "vector_math.hpp"

#include <gmock/gmock.h>

using testing::FloatNear;
const double kFloatDelta = .01;

TEST(vector_math, rotate_perpendicular_is_orthoganal) {
  glm::vec3 vec(2, 0, 0);
  glm::vec3 to_rotate(1, 3, 0);

  glm::vec3 rotated = rotate_to_perpendicular(to_rotate, vec);
  EXPECT_THAT(glm::dot(vec, rotated), FloatNear(0, .01));
}

TEST(vector_math, rotate_perpendicular_correct_direction) {
  glm::vec3 vec(2, 0, 0);
  glm::vec3 to_rotate(3, -1, 0);

  glm::vec3 rotated = rotate_to_perpendicular(to_rotate, vec);
  EXPECT_THAT(glm::dot(glm::vec3(0, -1, 0), rotated),
              FloatNear(glm::length(rotated), .01));
}

TEST(vector_math, rotate_perpendicular_preserves_length) {
  glm::vec3 vec(2, 3, 0);
  glm::vec3 to_rotate(5, 3, 0);

  glm::vec3 rotated = rotate_to_perpendicular(to_rotate, vec);
  EXPECT_THAT(rotated, IsNearLength(glm::length(to_rotate)));
}

TEST(vector_math, perpendicular_is_orthogonal) {
  glm::vec3 vec = glm::normalize(glm::vec3(2, 3, 1));
  glm::vec3 perp = perpendicular(vec);
  EXPECT_THAT(glm::dot(vec, perp), FloatNear(0, .001));
}

TEST(vector_math, trilerp_returns_correct_vector) {
  glm::vec3 x_vector(1, 0, 0);
  glm::vec3 y_vector(0, 2, 0);
  glm::vec3 z_vector(0, 0, 3);
  double c2 = -.75;
  double c3 = .25;
  glm::vec3 expected = y_vector * (float)c2 + z_vector * (float)c3;

  glm::vec3 lerped = trilerp(x_vector, y_vector, z_vector, c2, c3);
  EXPECT_THAT(lerped, VectorIsNear(expected));
}

TEST(vector_math, trislerp_returns_correct_length_vector_bases) {
  glm::vec3 x = glm::vec3(1, 0, 0);
  glm::vec3 y = glm::vec3(0, 1, 0);
  glm::vec3 z = glm::vec3(0, 0, 1);
  double c2 = .5;
  double c3 = .5;

  glm::vec3 slerped = trislerp(x, y, z, c2, c3);
  EXPECT_THAT(slerped, IsNearLength(glm::length(y) * .5 + glm::length(z) * .5));
}

TEST(vector_math, trislerp_returns_correct_length_vector_non_bases) {
  glm::vec3 v1 = glm::vec3(1, 0, 0);
  glm::vec3 v2 = glm::vec3(0, 0, 3);
  glm::vec3 v3 = glm::vec3(0, 2, 0);
  double c2 = .5;
  double c3 = .5;

  glm::vec3 slerped = trislerp(v1, v2, v3, c2, c3);
  EXPECT_THAT(slerped,
              IsNearLength(glm::length(v2) * .5 + glm::length(v3) * .5));
}

TEST(vector_math, trislerp_returns_correct_vector) {
  glm::vec3 x_vector(1, 0, 0);
  glm::vec3 y_vector(0, 2, 0);
  glm::vec3 z_vector(0, 0, 3);
  double c2 = 1.0 / 3;
  double c3 = 1.0 / 3;
  glm::vec3 expected(2 / std::sqrt(3), 2 / std::sqrt(3), 2 / std::sqrt(3));

  glm::vec3 slerped = trislerp(x_vector, y_vector, z_vector, c2, c3);
  EXPECT_THAT(slerped, VectorIsNear(expected));
}

TEST(vector_math, trislerp_handles_negative_coefficients) {
  glm::vec3 x_vector(1, 0, 0);
  glm::vec3 y_vector(0, 2, 0);
  glm::vec3 z_vector(0, 0, 3);
  double c2 = -1.0 / 3;
  double c3 = -1.0 / 3;
  glm::vec3 expected(2 / std::sqrt(3), -2 / std::sqrt(3), -2 / std::sqrt(3));

  glm::vec3 slerped = trislerp(x_vector, y_vector, z_vector, c2, c3);
  EXPECT_THAT(slerped, VectorIsNear(expected));
}
