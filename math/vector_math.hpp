#ifndef VECTOR_MATH_HPP
#define VECTOR_MATH_HPP

#include <glm/vec3.hpp>

// Rotates to_rotate away from away_from until it is perpendicular.
glm::vec3 rotate_to_perpendicular(glm::vec3 to_rotate, glm::vec3 away_from);

// Returns and arbitrary unit vector that is perpendicular to vec.
glm::vec3 perpendicular(glm::vec3 vec);

// Finds the trilinear interpolation of the given three vectors. If the given
// constants are negative, it interpolates with the negative corresponding
// vector.
glm::vec3 trilerp(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, double c2,
                  double c3);

// Finds the spherical interpolation between the given three vectors. Asserts
// that abs(c2) + abs(c3) <= 1.
glm::vec3 trislerp(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, double c2,
                   double c3);

#endif /* VECTOR_MATH_HPP */
