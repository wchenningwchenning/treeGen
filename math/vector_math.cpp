#include "vector_math.hpp"

#include <glm/gtx/projection.hpp>
#include <glm/gtx/rotate_vector.hpp>

glm::vec3 rotate_to_perpendicular(glm::vec3 to_rotate, glm::vec3 away_from) {
  glm::vec3 projection = glm::proj(to_rotate, away_from);
  glm::vec3 rejection = to_rotate - projection;
  return glm::normalize(rejection) * glm::length(to_rotate);
}

glm::vec3 perpendicular(glm::vec3 vec) {
  if (std::abs(vec.x) > std::abs(vec.y))
    return glm::normalize(glm::vec3(vec.z, 0, -vec.x));
  else
    return glm::normalize(glm::vec3(0, vec.z, -vec.y));
}

glm::vec3 trilerp(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, double c2,
                  double c3) {
  if (c2 < 0) {
    c2 *= -1;
    v2 *= -1;
  }
  if (c3 < 0) {
    c3 *= -1;
    v3 *= -1;
  }

  assert(c2 + c3 <= 1.01);

  double c1 = 1 - c2 - c3;
  return (float)c1 * v1 + (float)c2 * v2 + (float)c3 * v3;
}

glm::vec3 trislerp(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, double c2,
                   double c3) {
  if (c2 < 0) {
    c2 *= -1;
    v2 *= -1;
  }
  if (c3 < 0) {
    c3 *= -1;
    v3 *= -1;
  }

  assert(c2 + c3 <= 1.01);
  if (c2 + c3 == 0) {
    return v1;
  } else if (c3 == 1) {
    return v3;
  }

  double c1 = 1 - c2 - c3;
  double l1 = glm::length(v1);
  v1 /= l1;
  double l2 = glm::length(v2);
  v2 /= l2;
  double l3 = glm::length(v3);
  v3 /= l3;

  glm::vec3 v2_v3_slerped = glm::slerp(v2, v3, (float)(c3 / (c2 + c3)));
  glm::vec3 v1_v2_slerped = glm::slerp(v1, v2, (float)(c2 / (c1 + c2)));
  glm::vec3 v2_v3_slerped_plane_normal = glm::cross(v1, v2_v3_slerped);
  glm::vec3 v1_v2_slerped_plane_normal = glm::cross(v3, v1_v2_slerped);
  glm::vec3 trislerped = glm::normalize(
      glm::cross(v2_v3_slerped_plane_normal, v1_v2_slerped_plane_normal));
  glm::vec3 forward = v2_v3_slerped + v1_v2_slerped;
  if (glm::dot(trislerped, forward) < 0) {
    trislerped *= -1;
  }

  return trislerped * (float)(l1 * c1 + l2 * c2 + l3 * c3);
}
