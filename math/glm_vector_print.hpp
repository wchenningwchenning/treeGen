#ifndef VECTOR_PRINT_HPP
#define VECTOR_PRINT_HPP

#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <type_traits>
#include <utility>

/*
template <typename GLMType,
          typename = decltype(glm::to_string(std::declval<GLMType>()))>
std::ostream &operator<<(std::ostream &out, const GLMType &g) {
  return out << glm::to_string(g);
}*/

/*
template <typename GLMType>
std::ostream &operator<<(std::ostream &out, const GLMType &g) {
  return out << glm::to_string(g);
}*/

std::ostream &operator<<(std::ostream &out, const glm::vec3 &g) {
  return out << glm::to_string(g);
}

#endif /* VECTOR_PRINT_HPP */
