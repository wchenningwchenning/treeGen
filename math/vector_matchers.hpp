#ifndef VECTOR_MATCHERS_HPP
#define VECTOR_MATCHERS_HPP

#include "glm_vector_print.hpp"

#include <glm/glm.hpp>
#include <gmock/gmock.h>

namespace {
const double kFloatDelta = .01;
}

MATCHER_P(IsNearLength, length, "") {
  return abs(glm::length(arg) - length) <= kFloatDelta;
}
MATCHER_P(VectorIsNear, other, "") {
  return abs(arg.x - other.x) <= kFloatDelta &&
         abs(arg.y - other.y) <= kFloatDelta &&
         abs(arg.z - other.z) <= kFloatDelta;
}

#endif /*VECTOR_MATCHERS_HPP */
