#ifndef TREE_HPP
#define TREE_HPP

#include "node.hpp"
#include "species.hpp"

#include <iostream>
#include <memory>

// Code to procedurally generate trees
//
// Add growth yearly but track age as a decimal to implement leaf color change

typedef std::function<double(glm::vec3)> BrightnessGetter;
typedef std::function<glm::vec3(glm::vec3)> GradientGetter;
typedef std::function<void(glm::vec3, double)> BudPlacer;

class Tree {
  Species species;
  double age = 0;
  std::unique_ptr<Node> base = nullptr;
  BrightnessGetter light_brightness;
  GradientGetter light_gradient;
  BudPlacer place_bud;

  static double cached_exposure(Node *node);
  double recalculate_exposure(Node *node);
  void grow_year();

  // Grows the bud at this internode if it exists, otherwise asserts that food
  // is negligible.
  void grow_bud(Node *node, double food);

  // Grows this internode if it is not null, otherwise asserts that food is
  // negligible.
  void grow_node(Node *node, double food);

  void shed_branches(Node *node, double minimum_exposure_cutoff);

public:
  Tree(Species species, BrightnessGetter light_brightness,
       GradientGetter light_gradient, BudPlacer place_bud);

  void tick_years(double years);

  Node const *get_base_node() const { return base.get(); };

  void print_stats() const {
    std::cout << "Stats for tree: " << this << std::endl;
    std::cout << "Nodes: " << base->node_count() << std::endl;
    std::cout << "Buds: " << base->bud_count() << std::endl;
    std::cout << "Max depth: " << base->max_depth() << std::endl;
  }
};

#endif /* TREE_HPP */
