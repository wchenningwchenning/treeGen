#include "light.hpp"
#include <algorithm>

size_t Light::index(int x, int y, int z) const {
  return (x_size * y_size * z) + (x_size * y) + x;
}

void Light::coordinates_of_position(int *out_x, int *out_y, int *out_z,
                                    glm::vec3 position) const {
  *out_x = position.x / voxel_length + x_size / 2;
  *out_y = position.y / voxel_length + y_size / 2;
  *out_z = position.z / voxel_length;
}

bool Light::check_in_bounds(int x, int y, int z) const {
  return (x >= 0 && x < x_size) && (y >= 0 && y < y_size) &&
         (z >= 0 && z < z_size);
}

void Light::assert_in_bounds(int x, int y, int z) const {
  assert(check_in_bounds(x, y, z));
}

void Light::set_brightness(int x, int y, int z, double val) {
  assert_in_bounds(x, y, z);
  brightness[index(x, y, z)] = val;
}

void Light::increase_brightness(int x, int y, int z, double val) {
  assert_in_bounds(x, y, z);
  brightness[index(x, y, z)] += val;
}

double Light::get_brightness(int x, int y, int z) const {
  assert_in_bounds(x, y, z);
  return std::max(0.0f, brightness[index(x, y, z)]);
}

namespace {
double max_error_on_level(int level, double voxel_length, double bud_size) {
  double distance = level * voxel_length;
  return bud_size / (distance * distance);
}
} // namespace

void Light::add_bud(int x, int y, int z, double bud_size) {
  for (int level = 1; z - level >= 0; level++) {
    if (max_error_on_level(level, voxel_length, bud_size) < kBrightnessError) {
      break;
    }
    for (int dx = -level; dx <= level; dx++) {
      for (int dy = -level; dy <= level; dy++) {
        int curr_x = x + dx;
        int curr_y = y + dy;
        int curr_z = z - level;
        if (check_in_bounds(curr_x, curr_y, curr_z)) {
          increase_brightness(
              curr_x, curr_y, curr_z,
              -shadow_intensity(x, y, z, curr_x, curr_y, curr_z, bud_size));
        }
      }
    }
  }
}

namespace {
double cos_two_theta(double radius2, double height2) {
  double hyp2 = radius2 + height2;
  double cos2 = height2 / hyp2;
  double sin2 = radius2 / hyp2;
  return cos2 - sin2;
}
} // namespace

double Light::shadow_intensity(int bud_x, int bud_y, int bud_z, int voxel_x,
                               int voxel_y, int voxel_z,
                               double bud_size) const {
  int dx = voxel_x - bud_x;
  int dy = voxel_y - bud_y;
  int dz = voxel_z - bud_z;

  double squared_radius = dx * dx + dy * dy;
  double squared_height = dz * dz;
  double squared_distance = squared_radius + squared_height;

  if (squared_radius > squared_height) {
    return 0;
  }

  /*
  double cos_2t = cos_two_theta(squared_radius, squared_height);
  return bud_size * cos_2t / squared_distance;
  */
  return bud_size / squared_distance;
}

bool Light::check_in_bounds(glm::vec3 position) const {
  int x, y, z;
  coordinates_of_position(&x, &y, &z, position);
  return check_in_bounds(x, y, z);
}

Light::Light(int x_resolution, int y_resolution, int z_resolution,
             double voxel_length)
    : x_size(x_resolution), y_size(y_resolution), z_size(z_resolution),
      voxel_length(voxel_length) {
  assert(x_resolution % 2 == 0);
  assert(y_resolution % 2 == 0);
  assert(z_resolution % 2 == 0);
  brightness =
      std::vector<float>(x_resolution * y_resolution * z_resolution, 1);
}

void Light::add_bud(glm::vec3 position, double bud_size) {
  int x, y, z;
  coordinates_of_position(&x, &y, &z, position);
  add_bud(x, y, z, bud_size);
}

double Light::get_brightness(glm::vec3 position) const {
  int x, y, z;
  coordinates_of_position(&x, &y, &z, position);
  if (check_in_bounds(x, y, z)) {
    return get_brightness(x, y, z);
  } else {
    if (z >= 0) {
      return 1;
    } else {
      return 0;
    }
  }
}

namespace {
template <typename Func>
double grad(int x, int x_size, double voxel_length, Func brightness_func) {
  int first_x = std::max(x - 1, 0);
  int last_x = std::min(x + 1, x_size - 1);
  double dx = (last_x - first_x) * voxel_length;
  double dB_along_x = brightness_func(last_x) - brightness_func(first_x);
  double dBdx = dB_along_x / dx;
  return dBdx;
}
} // namespace

// Check the voxels neighboring the given position to calculate the gradient of
// the brightness field.
glm::vec3 Light::get_gradient(glm::vec3 position) const {
  int x, y, z;
  coordinates_of_position(&x, &y, &z, position);

  if (!check_in_bounds(x, y, z)) {
    return glm::vec3(0);
  }

  double dBdx = grad(x, x_size, voxel_length,
                     [this, y, z](int x) { return get_brightness(x, y, z); });
  double dBdy = grad(y, y_size, voxel_length,
                     [this, x, z](int y) { return get_brightness(x, y, z); });
  double dBdz = grad(z, z_size, voxel_length,
                     [this, x, y](int z) { return get_brightness(x, y, z); });
  return glm::vec3(dBdx, dBdy, dBdz);
}
