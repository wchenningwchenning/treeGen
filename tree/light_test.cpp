#include "light.hpp"
#include <gtest/gtest.h>

namespace {

TEST(Light, check_in_bounds) {
  // Makes a brightness field with x=[-5, 5], y=[-5, 5], z=[0,10]
  Light light(/*resolution=*/10, /*length=*/10);

  EXPECT_TRUE(light.check_in_bounds(glm::vec3(4, 0, 0)));
  EXPECT_TRUE(light.check_in_bounds(glm::vec3(0, -4, 0)));
  EXPECT_TRUE(light.check_in_bounds(glm::vec3(0, 0, 8)));

  EXPECT_FALSE(light.check_in_bounds(glm::vec3(8, 0, 0)));
  EXPECT_FALSE(light.check_in_bounds(glm::vec3(0, 0, -1)));
}

TEST(Light, add_bud_outside_field_casts_shadow) {
  const int kFieldHeight = 10;
  Light light(/*resolution=*/20, /*length=*/kFieldHeight);
  light.add_bud(glm::vec3(0, 0, kFieldHeight + 3), 100);

  EXPECT_LT(light.get_brightness(glm::vec3(glm::vec3(0, 0, kFieldHeight - 1))),
            1);
}

TEST(Light, shadow_is_45_deg) {
  Light light(/*resolution=*/40, /*length=*/20);
  const int kBudHeight = 8;
  light.add_bud(glm::vec3(0, 0, kBudHeight), 100);

  EXPECT_LT(light.get_brightness(glm::vec3(kBudHeight - 1, 0, 0)), 1);
  EXPECT_EQ(light.get_brightness(glm::vec3(kBudHeight + 1, 0, 0)), 1);
}

TEST(Light, shadow_gets_brighter_with_angle) {
  Light light(/*resolution=*/20, /*length=*/10);
  light.add_bud(glm::vec3(0, 0, 9.5), 5);

  EXPECT_GT(1 - light.get_brightness(glm::vec3(0, 0, 0)),
            1 - light.get_brightness(glm::vec3(1, 0, 0)));
  EXPECT_GT(1 - light.get_brightness(glm::vec3(0, 3, 0)),
            1 - light.get_brightness(glm::vec3(3, 3, 0)));
}

TEST(Light, shadow_gets_brighter_with_square_distance) {
  Light light(/*resolution=*/20, /*length=*/10);
  const int kBudHeight = 8;
  light.add_bud(glm::vec3(0, 0, kBudHeight), 1);

  double shadow_1_unit_away =
      1 - light.get_brightness(glm::vec3(0, 0, kBudHeight - 1));
  double shadow_2_units_away =
      1 - light.get_brightness(glm::vec3(0, 0, kBudHeight - 2));

  EXPECT_NEAR(shadow_1_unit_away, shadow_2_units_away * 4, .01);
}

TEST(Light, brightness_outside_field_is_1) {
  const int kFieldHeight = 10;
  Light light(/*resolution=*/20, /*length=*/kFieldHeight);
  light.add_bud(glm::vec3(0, 0, kFieldHeight + 3), 1000);

  EXPECT_EQ(light.get_brightness(glm::vec3(0, 0, kFieldHeight + 1)), 1);
}

TEST(Light, gradient_is_correct_on_each_axis) {
  const int kLength = 10;
  const int kResolution = 40;
  Light light(kResolution, kLength);
  light.add_bud(glm::vec3(0, 0, 5), 1);

  const double kPointX = 2.0;
  const double kPointY = 1.0;
  const double kPointZ = 1.0;
  glm::vec3 gradient = light.get_gradient(glm::vec3(kPointX, kPointY, kPointZ));

  const double kDV = (double)kLength / kResolution;

  EXPECT_EQ(gradient.x,
            (light.get_brightness(glm::vec3(kPointX + kDV, kPointY, kPointZ)) -
             light.get_brightness(glm::vec3(kPointX - kDV, kPointY, kPointZ))) /
                (2 * kDV));
  EXPECT_EQ(gradient.y,
            (light.get_brightness(glm::vec3(kPointX, kPointY + kDV, kPointZ)) -
             light.get_brightness(glm::vec3(kPointX, kPointY - kDV, kPointZ))) /
                (2 * kDV));
  EXPECT_EQ(gradient.z,
            (light.get_brightness(glm::vec3(kPointX, kPointY, kPointZ + kDV)) -
             light.get_brightness(glm::vec3(kPointX, kPointY, kPointZ - kDV))) /
                (2 * kDV));
}

TEST(Light, gradient_handles_edge_case) {
  const int kLength = 10;
  const int kResolution = 40;
  Light light(kResolution, kLength);
  light.add_bud(glm::vec3(-kLength, 0, 5), 1);

  const double kPointX = -kLength / 2 + .01;
  const double kPointY = 1;
  const double kPointZ = 0;
  glm::vec3 gradient = light.get_gradient(glm::vec3(kPointX, kPointY, kPointZ));

  const double kDV = (double)kLength / kResolution;

  EXPECT_EQ(gradient.x,
            (light.get_brightness(glm::vec3(kPointX + kDV, kPointY, kPointZ)) -
             light.get_brightness(glm::vec3(kPointX, kPointY, kPointZ))) /
                kDV);
}

} // namespace
