#include "node.hpp"
#include "skeleton_mesh.hpp"

#include <glm/gtx/rotate_vector.hpp>
#include <gtest/gtest.h>

TEST(SkeletonMesh, dp_node_count) {
  Node *base = new Node();
  Node *trunk_1 = new Node();
  Node *trunk_2 = new Node();
  Node *branch_1 = new Node();
  base->set_trunk(trunk_1);
  trunk_1->set_trunk(trunk_2);
  base->set_branch(branch_1);

  SkeletonMesh mesh(glm::vec3(0), nullptr, nullptr);
  EXPECT_EQ(mesh.dp_node_count(trunk_2), 1);
  EXPECT_EQ(mesh.dp_node_count(branch_1), 1);
  EXPECT_EQ(mesh.dp_node_count(base), 4);
  EXPECT_EQ(mesh.dp_node_count(trunk_1), 2);
  delete base;
}

TEST(SkeletonMesh, branch_radius_is_zero_at_end) {
  EXPECT_EQ(SkeletonMesh::branch_radius(1), 0);
}

TEST(SkeletonMesh, branch_radius_is_nonzero_before_end) {
  EXPECT_GT(SkeletonMesh::branch_radius(2), 0);
}

TEST(SkeletonMesh, branch_radius_increases_with_nodes) {
  EXPECT_GT(SkeletonMesh::branch_radius(10000),
            SkeletonMesh::branch_radius(5) * 4);
}

TEST(SkeletonMesh, branch_has_at_least_three_sides) {
  EXPECT_GE(SkeletonMesh::branch_segments(1), 3);
  EXPECT_GE(SkeletonMesh::branch_segments(100), 3);
}

TEST(SkeletonMesh, larger_branches_have_more_edges) {
  EXPECT_GE(SkeletonMesh::branch_segments(10000),
            SkeletonMesh::branch_segments(20) * 2);
}

TEST(SkeletonMesh, half_of_added_cone_vertices_are_at_base) {
  std::vector<Vertex> vertices;
  std::vector<uint32_t> indices;

  Node *base = new Node(glm::vec3(0, 0, 1), glm::vec3(0), nullptr);
  Node *trunk = new Node(glm::vec3(0, 0, 1), glm::vec3(0, 0, 1), nullptr);
  base->set_trunk(trunk);

  SkeletonMesh::add_tree(glm::vec3(0), &vertices, &indices, base);

  std::vector<glm::vec3> points;
  int start_points = 0;
  for (const auto &v : vertices) {
    points.push_back(v.pos);
    if (v.pos.z == 0) {
      start_points++;
    }
  }
  EXPECT_EQ(start_points, points.size() / 2);
  delete base;
}

TEST(SkeletonMesh, cone_triangle_has_right_rotation) {
  std::vector<Vertex> vertices;
  std::vector<uint32_t> indices;

  Node *base = new Node(glm::vec3(0, 0, 1), glm::vec3(0), nullptr);
  Node *trunk_1 = new Node(glm::vec3(0, 0, 1), glm::vec3(0, 0, 1), nullptr);
  base->set_trunk(trunk_1);

  SkeletonMesh::add_tree(glm::vec3(0), &vertices, &indices, base);

  std::vector<glm::vec3> first_tri;
  first_tri.push_back(vertices[indices[0]].pos);
  first_tri.push_back(vertices[indices[1]].pos);
  first_tri.push_back(vertices[indices[2]].pos);

  int segments = vertices.size() / 4;
  float dot_prod = std::cos(2 * M_PI / segments);

  std::vector<std::pair<int, int>> subsets = {{0, 1}, {1, 2}, {0, 2}};
  for (std::pair<int, int> vals : subsets) {
    const glm::vec3 t1 = glm::normalize(first_tri[vals.first]);
    const glm::vec3 t2 = glm::normalize(first_tri[vals.second]);
    if (t1.z == t2.z) {
      EXPECT_NEAR(glm::dot(t1, t2), dot_prod, .01);
    }
  }
  delete base;
}
