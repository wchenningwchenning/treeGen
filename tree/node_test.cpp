#include "bud.hpp"
#include "node.hpp"
#include "species.hpp"

#include <glm/vec3.hpp>
#include <gtest/gtest.h>

TEST(Node, has_bud_with_bud) {
  Bud *bud = new Bud(glm::vec3(0, 0, 1), glm::vec3(1, 0, 0), true);
  Node node(glm::vec3(0, 0, 1), glm::vec3(0), bud);
  EXPECT_TRUE(node.has_bud());
}

TEST(Node, has_bud_without_bud) {
  Node node(glm::vec3(0, 0, 1), glm::vec3(0), nullptr);
  EXPECT_FALSE(node.has_bud());
}

TEST(Node, has_terminal_bud) {
  Bud *terminal_bud =
      new Bud(glm::vec3(0, 0, 1), glm::vec3(1, 0, 0), /*is_terminal=*/true);
  Bud *axial_bud =
      new Bud(glm::vec3(0, 0, 1), glm::vec3(1, 0, 0), /*is_terminal=*/false);
  Node node_with_terminal_bud(glm::vec3(0, 0, 1), glm::vec3(0), terminal_bud);
  Node node_with_axial_bud(glm::vec3(0, 0, 1), glm::vec3(0), axial_bud);
  Node node_with_no_bud(glm::vec3(0, 0, 1), glm::vec3(0), nullptr);
  EXPECT_TRUE(node_with_terminal_bud.has_terminal_bud());
  EXPECT_FALSE(node_with_axial_bud.has_terminal_bud());
  EXPECT_FALSE(node_with_no_bud.has_terminal_bud());
}

TEST(Node, has_axial_bud) {
  Bud *terminal_bud =
      new Bud(glm::vec3(0, 0, 1), glm::vec3(1, 0, 0), /*is_terminal=*/true);
  Bud *axial_bud =
      new Bud(glm::vec3(0, 0, 1), glm::vec3(1, 0, 0), /*is_terminal=*/false);
  Node node_with_terminal_bud(glm::vec3(0, 0, 1), glm::vec3(0), terminal_bud);
  Node node_with_axial_bud(glm::vec3(0, 0, 1), glm::vec3(0), axial_bud);
  Node node_with_no_bud(glm::vec3(0, 0, 1), glm::vec3(0), nullptr);
  EXPECT_FALSE(node_with_terminal_bud.has_axial_bud());
  EXPECT_TRUE(node_with_axial_bud.has_axial_bud());
  EXPECT_FALSE(node_with_no_bud.has_terminal_bud());
}

TEST(Node, has_outgoing_node) {
  Node base(*Node::new_tree(glm::vec3(0)));

  EXPECT_FALSE(base.has_outgoing_node());

  Node *branch = Node::new_tree(glm::vec3(0));
  base.set_branch(branch);

  EXPECT_TRUE(base.has_outgoing_node());
}

namespace {
Node *new_node_with_bud() {
  return new Node(glm::vec3(0), glm::vec3(0),
                  new Bud(glm::vec3(0), glm::vec3(0), false));
}

Node *new_node_without_bud() {
  return new Node(glm::vec3(0), glm::vec3(0), nullptr);
}
} // namespace

TEST(Node, node_count) {
  Node *base = new_node_without_bud();
  Node *trunk_1 = new_node_with_bud();
  Node *trunk_2 = new_node_with_bud();
  base->set_trunk(trunk_1);
  trunk_1->set_trunk(trunk_2);
  Node *branch = new_node_with_bud();
  base->set_branch(branch);

  EXPECT_EQ(base->node_count(), 4);
  delete base;
}

TEST(Node, bud_count) {
  Node *base = new_node_without_bud();
  Node *trunk_1 = new_node_with_bud();
  Node *trunk_2 = new_node_with_bud();
  base->set_trunk(trunk_1);
  trunk_1->set_trunk(trunk_2);
  Node *branch = new_node_with_bud();
  base->set_branch(branch);

  EXPECT_EQ(base->bud_count(), 3);
  delete base;
}

TEST(Node, max_depth) {
  Node *base = new_node_without_bud();
  Node *trunk_1 = new_node_with_bud();
  Node *trunk_2 = new_node_with_bud();
  base->set_trunk(trunk_1);
  trunk_1->set_trunk(trunk_2);
  Node *branch_1 = new_node_without_bud();
  Node *branch_2 = new_node_without_bud();
  Node *branch_3 = new_node_with_bud();
  base->set_branch(branch_1);
  branch_1->set_trunk(branch_2);
  branch_2->set_trunk(branch_3);

  EXPECT_EQ(base->max_depth(), 4);
  delete base;
}
