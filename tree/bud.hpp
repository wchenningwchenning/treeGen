#ifndef BUD_HPP
#define BUD_HPP

#include "species.hpp"

#include <cassert>
#include <glm/vec3.hpp>
#include <utility>

class Node;

class Bud {
  glm::vec3 direction; // Possibly unneccessary in the way position is
  glm::vec3 rotation_coaxis;
  // double years_since_growth;  // Very unsure where this would come in
  bool terminal;
  // int order;  // Not sure if this would make things simpler

  void reorient(glm::vec3 light_gradient, const BranchGrowth &branch_growth);
  void rotate(const BranchGrowth &branch_growth);

public:
  Bud(glm::vec3 direction, glm::vec3 rotation_coaxis, bool is_terminal)
      : direction(direction), rotation_coaxis(rotation_coaxis),
        terminal(is_terminal){};

  // Grows the current bud returning the shoot of growth and keeping the current
  // bud at the end of that shoot. If an axial bud is grown, that is returned as
  // well.
  std::pair<Node *, Bud *>
  grow_shoot_and_bud(glm::vec3 position, glm::vec3 light_gradient,
                     const BranchGrowth &branch_growth);

  glm::vec3 get_direction() const { return direction; };
  glm::vec3 get_rotation_coaxis() const { return rotation_coaxis; };
  bool is_terminal() const { return terminal; };
  bool is_axial() const { return !is_terminal(); };

  // glm::vec3 get_direction() const { return direction; }
  // glm::vec3 get_rotation_coaxis() const { return rotation_coaxis; }
  void set_rotation_coaxis(glm::vec3 coaxis) { rotation_coaxis = coaxis; }
};

#endif /* BUD_HPP */
