#include "tree/skeleton_mesh.hpp"
#include "math/vector_math.hpp"
#include <glm/gtx/rotate_vector.hpp>

int SkeletonMesh::dp_node_count(const Node *node) {
  if (!node)
    return 0;

  if (node_count.count(node) != 0) {
    return node_count[node];
  } else {
    node_count[node] = 1 + node->get_num_shed_branches() +
                       dp_node_count(node->get_trunk()) +
                       dp_node_count(node->get_branch());
    return node_count[node];
  }
}

void SkeletonMesh::add_cone(glm::vec3 start_pos, float start_radius,
                            glm::vec3 end_pos, float end_radius, int segments,
                            glm::vec3 color) {
  float angle = 2 * M_PI / segments;
  glm::vec3 base = end_pos - start_pos;
  glm::vec3 unit_radius = perpendicular(base);

  glm::vec3 curr_radius = unit_radius;
  for (int i = 0; i < segments; ++i) {
    glm::vec3 next_radius = glm::rotate(curr_radius, angle, base);
    vertices->push_back(
        {offset + start_pos + curr_radius * start_radius, color});
    vertices->push_back(
        {offset + start_pos + next_radius * start_radius, color});
    vertices->push_back({offset + end_pos + curr_radius * end_radius, color});
    vertices->push_back({offset + end_pos + next_radius * end_radius, color});

    indices->push_back(vertices->size() - 4);
    indices->push_back(vertices->size() - 3);
    indices->push_back(vertices->size() - 2);
    indices->push_back(vertices->size() - 2);
    indices->push_back(vertices->size() - 3);
    indices->push_back(vertices->size() - 1);

    curr_radius = next_radius;
  }
}
