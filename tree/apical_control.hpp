#ifndef APICAL_CONTROL_HPP
#define APICAL_CONTROL_HPP

class ApicalControl {
  const double kBranchBias = .5;

public:
  ApicalControl(double branch_bias) : kBranchBias(branch_bias){};
  double branch_bias(double /*age*/) { return kBranchBias; }
};

#endif /* APICAL_CONTROL_HPP */
