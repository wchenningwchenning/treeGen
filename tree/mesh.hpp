#ifndef TREEGEN_HPP
#define TREEGEN_HPP

// Generates a mesh from a tree structure

#include "tree/grove.hpp"
#include "tree/skeleton_mesh.hpp"
#include "tree/tree.hpp"

#include <glm/vec3.hpp>
#include <vector>

class Mesh {
  void add_tree(const Tree &tree, glm::vec3 position) {
    SkeletonMesh::add_tree(position, &vertices, &indices, tree.get_base_node());
  }

public:
  std::vector<Vertex> vertices;
  std::vector<uint32_t> indices;

  static Mesh from_tree(const Tree &tree) {
    Mesh mesh;
    mesh.add_tree(tree, glm::vec3(0));
    return mesh;
  }

  static Mesh from_grove(const Grove &grove) {
    Mesh grove_mesh;

    for (size_t i = 0; i < grove.size(); ++i) {
      glm::vec3 tree_pos = grove.get_tree_position(i);
      grove_mesh.add_tree(grove.get_tree(i), tree_pos);
    }

    return grove_mesh;
  }
};

#endif /* TREEGEN_HPP */
