#ifndef GROVE_HPP
#define GROVE_HPP

#include "light.hpp"
#include "tree.hpp"

#include <vector>

class Grove {
  const double kLightVoxelLength = .25;

  std::vector<Tree> trees;
  std::vector<glm::vec3> tree_positions;
  Light light_volume;
  glm::vec3 light_volume_origin; // In grove space
  double year;

  void grow_year();
  glm::vec3 light_volume_coord_from_local_coord(glm::vec3 local_origin,
                                                glm::vec3 local_point) const;

public:
  // Uses the same indexing as get_tree_position. Trees are not guaranteed to be
  // returned in any order.
  const Tree &get_tree(int i) const { return trees[i]; }

  // Returns the origin of a tree in grove coordinates. Uses the same indexing
  // as get_tree.
  glm::vec3 get_tree_position(int i) const { return tree_positions[i]; }

  size_t size() const { return trees.size(); }

  // Adds a new tree sapling of the given species to this grove at the given
  // position in grove coordinates.
  void add_tree(glm::vec3 position, Species species);

  // Returns a function that returns the brightness of the grove when given
  // coordinates in a local coordinate system.
  BrightnessGetter
  local_brightness_getter(glm::vec3 local_origin /*in_grove_space*/) const;

  // Returns a function that returns the brightness gradient of the grove when
  // given coordinates in a local coordinate system.
  GradientGetter
  local_gradient_getter(glm::vec3 local_origin /*in_grove_space*/) const;

  // Returns a function that places buds in the grove when given coordinates in
  // a local coordinate system.
  BudPlacer local_bud_placer(glm::vec3 local_origin /*in_grove_space*/);

  void tick_years(double years);
  void print_stats() {
    for (const Tree &tree : trees)
      tree.print_stats();
  }
  Grove(double x_length, double y_length, double light_volume_height);
};

#endif /* GROVE_HPP */
