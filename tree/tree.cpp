#include "tree/tree.hpp"
#include "math/vector_math.hpp"
#include "tree/bud.hpp"
#include "tree/node.hpp"

double Tree::cached_exposure(Node *node) { return node->get_exposure(); }

double Tree::recalculate_exposure(Node *node) {
  if (!node)
    return 0;

  double exposure = 0;
  exposure += recalculate_exposure(node->get_trunk());
  exposure += recalculate_exposure(node->get_branch());
  if (node->has_bud())
    exposure += light_brightness(node->get_position());
  node->set_exposure(exposure);
  return exposure;
}

void Tree::shed_branches(Node *node, double minimum_exposure_cutoff) {}

void Tree::grow_bud(Node *node, double food) {
  assert(node);
  assert(food >= 0);
  if (!node->has_bud()) {
    assert(food < .001);
    return;
  }

  for (int metamers = ceil(food); metamers >= 1; --metamers) {
    node->assert_invariants();
    Bud *bud = node->get_bud();
    bool bud_was_terminal = node->bud_is_terminal();

    glm::vec3 position = node->get_position();
    std::pair<Node *, Bud *> shoot_and_bud = bud->grow_shoot_and_bud(
        position, light_gradient(position), species.branch_growth);

    Bud *new_bud = shoot_and_bud.second;
    node->set_bud(new_bud);
    if (new_bud) {
      place_bud(node->get_position(), .7);
    }

    if (bud_was_terminal) {
      node->set_trunk(shoot_and_bud.first);
      node->assert_invariants();
    } else {
      node->set_branch(shoot_and_bud.first);
      node->assert_invariants();
    }
    node = shoot_and_bud.first;
  }
}

namespace {
double divide_resources(double trunk_exposure, double branch_exposure,
                        double food, double trunk_bias) {
  double denominator =
      trunk_bias * trunk_exposure + (1 - trunk_bias) * branch_exposure;
  if (denominator <= 0) {
    return 0;
  }

  double trunk_proportion = trunk_bias * trunk_exposure / denominator;
  return food * trunk_proportion;
}
} // namespace

void Tree::grow_node(Node *node, double food) {
  assert(food >= 0);
  if (!node) {
    assert(food <= .001);
    return;
  }

  // Valid input cases:
  //  #  apical terminal trunk branch
  //  1  0      1        0     0
  //  2  1      0        0     0
  //  3  1      0        1     0
  //  4  0      0        1     1
  //  5  0      0        0     1
  //  6  0      0        1     0
  //  7  0      0        0     0

  // Cases: 1 2
  // This node has a bud and no outgoing nodes
  if (node->has_bud() && !node->has_outgoing_node()) {
    grow_bud(node, food);
    return;
  }

  // Case: 3
  // This node has an axial bud and an outgoing trunk edge
  double trunk_bias = 1 - species.apical_control.branch_bias(age);
  double trunk_exposure = cached_exposure(node->get_trunk());
  if (node->has_axial_bud() && node->get_trunk()) {
    double bud_exposure = node->get_exposure() - trunk_exposure;
    double trunk_food =
        divide_resources(trunk_exposure, bud_exposure, food, trunk_bias);
    grow_node(node->get_trunk(), trunk_food);
    grow_bud(node, food - trunk_food);
    return;
  }

  // Cases: 4 5 6 7
  // This node has no buds
  assert(!node->has_bud());
  double branch_exposure = cached_exposure(node->get_branch());
  double trunk_food =
      divide_resources(trunk_exposure, branch_exposure, food, trunk_bias);
  grow_node(node->get_trunk(), trunk_food);
  grow_node(node->get_branch(), food - trunk_food);
}

void Tree::grow_year() {
  recalculate_exposure(base.get());
  grow_node(base.get(), base->get_exposure());
  shed_branches(base.get(), species.kBranchShedConstant);
}

Tree::Tree(Species species, std::function<double(glm::vec3)> light_brightness,
           std::function<glm::vec3(glm::vec3)> light_gradient,
           BudPlacer place_bud)
    : species(species), light_brightness(light_brightness),
      light_gradient(light_gradient), place_bud(place_bud) {
  base = std::unique_ptr<Node>(Node::new_tree(glm::vec3(0)));
}

void Tree::tick_years(double years) {
  // For each season:
  //   Leaf to base calculate exposures
  //   Base to leaf growth with stored exposures and apical control
  //   TODO implement branch shedding

  int years_to_grow = (int)age + years - (int)age;
  age = std::ceil(age);
  for (int year = 0; year < years_to_grow; ++year) {
    grow_year();
    age++;
  }
  age = years;
  // assert(false); // TODO branch shedding
}
