#include "grove.hpp"
#include "light.hpp"

#include <cmath>

void Grove::add_tree(glm::vec3 tree_position, Species species) {
  trees.push_back(Tree(species, local_brightness_getter(tree_position),
                       local_gradient_getter(tree_position),
                       local_bud_placer(tree_position)));
  tree_positions.push_back(tree_position);
}

glm::vec3
Grove::light_volume_coord_from_local_coord(glm::vec3 local_origin,
                                           glm::vec3 local_point) const {
  glm::vec3 grove_coord_of_point = local_origin + local_point;
  return grove_coord_of_point - light_volume_origin;
}

BrightnessGetter Grove::local_brightness_getter(
    glm::vec3 local_origin /*in_grove_space*/) const {
  return [this, local_origin](glm::vec3 point) {
    return light_volume.get_brightness(
        light_volume_coord_from_local_coord(local_origin, point));
  };
}

GradientGetter
Grove::local_gradient_getter(glm::vec3 local_origin /*in_grove_space*/) const {
  return [this, local_origin](glm::vec3 point) {
    return light_volume.get_gradient(
        light_volume_coord_from_local_coord(local_origin, point));
  };
}

BudPlacer Grove::local_bud_placer(glm::vec3 local_origin /*in_grove_space*/) {
  return [this, local_origin](glm::vec3 point, double size) {
    light_volume.add_bud(
        light_volume_coord_from_local_coord(local_origin, point), size);
  };
}

void Grove::tick_years(double years) {
  for (auto &tree : trees) {
    tree.tick_years(years);
  }
}

namespace {
Light light_from_dimensions(double x_length, double y_length,
                            double light_volume_height,
                            double light_voxel_length) {
  return Light(2 * ceil(x_length / light_voxel_length / 2.0),
               2 * ceil(y_length / light_voxel_length / 2.0),
               2 * ceil(light_volume_height / light_voxel_length / 2.0),
               light_voxel_length);
}
} // namespace

Grove::Grove(double x_length, double y_length, double light_volume_height)
    : light_volume(light_from_dimensions(
          2 * x_length, 2 * y_length, light_volume_height, kLightVoxelLength)) {
  light_volume_origin = glm::vec3(
      ceil(x_length / kLightVoxelLength / 2.0) * kLightVoxelLength,
      ceil(y_length / kLightVoxelLength / 2.0) * kLightVoxelLength, 0);
}
