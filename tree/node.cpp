#include "node.hpp"
#include "bud.hpp"

void Node::assert_invariants() const {
  assert(!get_branch() || !has_axial_bud());
  assert(!get_trunk() || !has_terminal_bud());
  assert(get_trunk() != this);
  assert(get_branch() != this);
}

bool Node::bud_is_terminal() const {
  assert(has_bud());
  return bud->is_terminal();
}

bool Node::bud_is_axial() const { return !bud_is_terminal(); }

size_t Node::node_count() const {
  size_t size = 1;
  if (next_node)
    size += next_node->node_count();
  if (branched_node)
    size += branched_node->node_count();
  return size;
}

size_t Node::bud_count() const {
  size_t size = 0;
  if (bud)
    size += 1;
  if (next_node)
    size += next_node->bud_count();
  if (branched_node)
    size += branched_node->bud_count();
  return size;
}

size_t Node::max_depth() const {
  size_t depth = 0;
  if (next_node)
    depth = std::max(depth, next_node->max_depth());
  if (branched_node)
    depth = std::max(depth, branched_node->max_depth());
  return depth + 1;
}

Node::~Node() {
  delete next_node;
  delete branched_node;
  delete bud;
}

Node *Node::new_tree(glm::vec3 position) {
  glm::vec3 up(0, 0, 1);
  // TODO, might want rotation coaxis to be random.
  Bud *first_bud = new Bud(/*direction=*/up, /*coaxis=*/glm::vec3(1, 0, 0),
                           /*is_terminal=*/true);
  Node *tree = new Node(/*direction=*/up, position, first_bud);
  return tree;
}
