#ifndef PHYLLOTAXIS_HPP
#define PHYLLOTAXIS_HPP

#include <glm/vec3.hpp>
#include <math.h>

class Phyllotaxis {
  // The rotation angle, in radians, around the stem between neighboring buds.
  const double kRotationAngle;
  // The angle, in radians, that a bud diverges from the main branch.
  const double kBranchAngle;
  // A decimal with 0.0 meaning buds don't rotate at all, and 1.0 meaning buds
  // rotate to horizontal plane.
  const double kRotateToHorizontal;

public:
  Phyllotaxis(double rotation_angle, double branch_angle,
              double rotate_to_horizontal)
      : kRotationAngle(rotation_angle), kBranchAngle(branch_angle),
        kRotateToHorizontal(rotate_to_horizontal) {}

  static Phyllotaxis basic() {
    return Phyllotaxis(/*rotation_angle=*/2 * M_PI * 37.5 / 360.0,
                       /*branch_angle=*/M_PI / 3,
                       /*rotate_to_horizontal=*/.3);
  };

  glm::vec3 rotate_coaxis(glm::vec3 trunk_axis, glm::vec3 coaxis) const;
  glm::vec3 branch_direction(glm::vec3 trunk_axis, glm::vec3 coaxis) const;
  glm::vec3 branch_coaxis(glm::vec3 trunk_axis, glm::vec3 branch_axis) const;
};

#endif /* PHYLLOTAXIS_HPP */
