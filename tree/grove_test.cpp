#include "grove.hpp"
#include "node.hpp"
#include "species.hpp"

#include <gtest/gtest.h>

TEST(grove, default_brightness) {
  Grove grove(20, 20, 20);
  BrightnessGetter brightness_no_offset =
      grove.local_brightness_getter(glm::vec3(0));
  BrightnessGetter brightness_mid_offset =
      grove.local_brightness_getter(glm::vec3(10));

  EXPECT_EQ(brightness_no_offset(glm::vec3(1, 1, 1)), 1);
  EXPECT_EQ(brightness_no_offset(glm::vec3(19, 19, 19)), 1);
  EXPECT_EQ(brightness_no_offset(glm::vec3(500, 500, 600)), 1);

  EXPECT_EQ(brightness_mid_offset(glm::vec3(0, 0, 0)), 1);
  EXPECT_EQ(brightness_mid_offset(glm::vec3(20, 20, 20)), 1);
}

TEST(grove, place_bud_without_offset) {
  Grove grove(20, 20, 20);
  BrightnessGetter brightness_no_offset =
      grove.local_brightness_getter(glm::vec3(0));
  BrightnessGetter brightness_mid_offset =
      grove.local_brightness_getter(glm::vec3(10));
  BudPlacer place_bud_without_offset = grove.local_bud_placer(glm::vec3(0));

  place_bud_without_offset(glm::vec3(1, 1, 5), 100000);
  EXPECT_EQ(brightness_no_offset(glm::vec3(1, 1, 3)), 0);
  EXPECT_EQ(brightness_no_offset(glm::vec3(1, 1, 7)), 1);
  EXPECT_EQ(brightness_mid_offset(glm::vec3(-9, -9, -7)), 0);
  EXPECT_EQ(brightness_mid_offset(glm::vec3(-9, -9, -3)), 1);

  place_bud_without_offset(glm::vec3(19, 19, 17), 100000);
  EXPECT_EQ(brightness_no_offset(glm::vec3(19, 19, 15)), 0);
  EXPECT_EQ(brightness_no_offset(glm::vec3(19, 19, 19)), 1);
  EXPECT_EQ(brightness_mid_offset(glm::vec3(9, 9, 5)), 0);
  EXPECT_EQ(brightness_mid_offset(glm::vec3(9, 9, 9)), 1);
}

TEST(grove, place_bud_with_offset) {
  Grove grove(20, 20, 20);
  BrightnessGetter brightness_no_offset =
      grove.local_brightness_getter(glm::vec3(0));
  BrightnessGetter brightness_mid_offset =
      grove.local_brightness_getter(glm::vec3(10));
  BudPlacer place_bud_with_offset = grove.local_bud_placer(glm::vec3(10));

  place_bud_with_offset(glm::vec3(-9, -9, -7), 100000);
  EXPECT_EQ(brightness_no_offset(glm::vec3(1, 1, 1)), 0);
  EXPECT_EQ(brightness_no_offset(glm::vec3(1, 1, 5)), 1);
  EXPECT_EQ(brightness_mid_offset(glm::vec3(-9, -9, -9)), 0);
  EXPECT_EQ(brightness_mid_offset(glm::vec3(-9, -9, -5)), 1);

  place_bud_with_offset(glm::vec3(9, 9, 7), 100000);
  EXPECT_EQ(brightness_no_offset(glm::vec3(19, 19, 15)), 0);
  EXPECT_EQ(brightness_no_offset(glm::vec3(19, 19, 19)), 1);
  EXPECT_EQ(brightness_mid_offset(glm::vec3(9, 9, 5)), 0);
  EXPECT_EQ(brightness_mid_offset(glm::vec3(9, 9, 9)), 1);
}
