#ifndef SPECIES_HPP
#define SPECIES_HPP

// A struct that holds the info on the breed of a tree
//
// Important details of a tree:
// Proleptic or Sylleptic
//   One or the other
// Phyllotaxis
//   Rotation rate
//   Horizontal bias
// Apical control constant
//   L in [0, 1] with L > 0.5 biased towards main branch
//   and L < 0.5 biased towards lateral branch
//   L is some function of time
// Branch diameter constants
//   d^n = d1^n + d2^n
//   some exponent n usually in [2,3]
// Branch shedding constants
//   Resources / Internode cutoff
// Shoot growth direction constants
//   Phototropism, e, positive
//   Gravitropism, n, positive or negative
//   Possibly vary tropism with branch order and/or time
// Leaf info? (Possibly a rendering detail)
//   Size, shape, and color of leaves and flowers?
// Bark info?
//   Color and texture of the branches?
//

#include "apical_control.hpp"
#include "phyllotaxis.hpp"

#include <memory>

class BranchGrowth {
  const Phyllotaxis phyllotaxis;
  const double kPhototropism;
  const double kGravitropism;
  const double kMetamerLength;

public:
  BranchGrowth(Phyllotaxis phyllotaxis, double phototropism,
               double gravitropism, double metamer_length)
      : phyllotaxis(phyllotaxis), kPhototropism(phototropism),
        kGravitropism(gravitropism), kMetamerLength(metamer_length){};

  static BranchGrowth basic() {
    return BranchGrowth(Phyllotaxis::basic(), /*phototropism=*/.2,
                        /*gravitropism=*/.05, /*metamer_length=*/.5);
  }

  inline double phototropism() const { return kPhototropism; }
  inline double gravitropism() const { return kGravitropism; }
  inline double metamer_length() const { return kMetamerLength; }
  inline const Phyllotaxis &get_phyllotaxis() const { return phyllotaxis; }
};

class Species {
  friend class Tree;
  friend class Bud;
  // const bool kIsProleptic; // TODO implement
  ApicalControl apical_control;

public:
  BranchGrowth branch_growth;
  const double kBranchShedConstant;

  Species()
      : apical_control(ApicalControl(.42)),
        branch_growth(BranchGrowth::basic()), kBranchShedConstant(.75){};

  Species(ApicalControl apical_control, BranchGrowth branch_growth,
          double branch_shed_constant)
      : apical_control(apical_control), branch_growth(branch_growth),
        kBranchShedConstant(branch_shed_constant){};
};

#endif /* SPECIES_HPP */
