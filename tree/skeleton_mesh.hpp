#ifndef SKELETON_MESH_HPP
#define SKELETON_MESH_HPP

#include "rendering/vertex.hpp"
#include "tree/node.hpp"

#include <unordered_map>
#include <vector>

class SkeletonMesh {
  constexpr static double max_error = .01;
  constexpr static double radius_constant = 1 / 100.0;

  glm::vec3 offset;
  std::vector<Vertex> *vertices;
  std::vector<uint32_t> *indices;
  std::unordered_map<const Node *, int> node_count;

  void add_branch(const Node *curr_node, const Node *next_node) {
    assert(curr_node);
    if (!next_node) {
      return;
    }
    double start_radius = branch_radius(dp_node_count(curr_node));
    double end_radius = branch_radius(dp_node_count(next_node));
    glm::vec3 start_position = curr_node->get_position();
    glm::vec3 end_position;
    if (next_node)
      end_position = next_node->get_position();
    else
      end_position = curr_node->get_position() + curr_node->get_direction();

    add_cone(start_position, start_radius, end_position, end_radius,
             branch_segments(start_radius), glm::vec3(0));
  }

  void add_node(const Node *node) {
    if (!node)
      return;
    add_branch(node, node->get_trunk());
    add_branch(node, node->get_branch());
    add_node(node->get_trunk());
    add_node(node->get_branch());
  }

public:
  SkeletonMesh(glm::vec3 offset, std::vector<Vertex> *vertices,
               std::vector<uint32_t> *indices)
      : offset(offset), vertices(vertices), indices(indices){};

  int dp_node_count(const Node *node);

  static double branch_radius(int nodes) {
    return std::sqrt(nodes - 1) * radius_constant;
  }

  static int branch_segments(int nodes) {
    return std::max(3, (int)(2 * std::sqrt(2) *
                                 std::sqrt(branch_radius(nodes) / max_error) +
                             1));
  }

  void add_cone(glm::vec3 start_pos, float start_radius, glm::vec3 end_pos,
                float end_radius, int segments, glm::vec3 color);

  static void add_tree(glm::vec3 pos, std::vector<Vertex> *vertices,
                       std::vector<uint32_t> *indices, const Node *base) {
    SkeletonMesh mesh(pos, vertices, indices);
    mesh.add_node(base);
    /*
    for(const auto& vert : DoublePlane::vertices) {
      vertices->push_back({vert.pos + pos, vert.color});
    }

    for(auto i : DoublePlane::indices) {
      indices->push_back(i);
    }*/
  }
};

#endif /* SKELETON_MESH_HPP */
