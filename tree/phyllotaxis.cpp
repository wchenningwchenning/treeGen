#include "tree/phyllotaxis.hpp"
#include "math/vector_math.hpp"
#include "tree/bud.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/vector_angle.hpp>

glm::vec3 Phyllotaxis::rotate_coaxis(glm::vec3 trunk_axis,
                                     glm::vec3 coaxis) const {
  return glm::rotate(coaxis, (float)kRotationAngle, trunk_axis);
}

glm::vec3 Phyllotaxis::branch_direction(glm::vec3 trunk_axis,
                                        glm::vec3 coaxis) const {
  glm::vec3 normal = glm::cross(trunk_axis, coaxis);
  glm::vec3 branch = glm::rotate(trunk_axis, (float)kBranchAngle, normal);
  // Prevent divide by zero when branch ends up vertical
  if (branch.x == 0 && branch.y == 0) {
    branch.x += .001;
  }
  glm::vec3 horizontal = glm::normalize(glm::vec3(branch.x, branch.y, 0));
  return glm::mix(branch, horizontal, (float)kRotateToHorizontal);
}

glm::vec3 Phyllotaxis::branch_coaxis(glm::vec3 trunk_axis,
                                     glm::vec3 branch_axis) const {
  return rotate_to_perpendicular(trunk_axis, branch_axis);
}
