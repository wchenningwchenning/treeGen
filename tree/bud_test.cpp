#include "math/vector_matchers.hpp"
#include "math/vector_math.hpp"
#include "tree/bud.hpp"
#include "tree/node.hpp"
#include "tree/phyllotaxis.hpp"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

TEST(Bud, grown_shoot_is_correct_length) {
  Bud *bud =
      new Bud(/*direction=*/glm::vec3(1, 0, 0), /*coaxis=*/glm::vec3(0, 0, 1),
              /*is_terminal=*/true);
  double metamer_length = .15;
  BranchGrowth growth = BranchGrowth(Phyllotaxis::basic(), /*gravitropism=*/.2,
                                     /*phototropism=*/.4, metamer_length);

  std::pair<Node *, Bud *> shoot_and_bud =
      bud->grow_shoot_and_bud(/*position=*/glm::vec3(0, 10, 0),
                              /*light_gradient=*/glm::vec3(0, 0, -1), growth);

  EXPECT_THAT(shoot_and_bud.first->get_direction(),
              IsNearLength(metamer_length));
  delete shoot_and_bud.first;
  delete shoot_and_bud.second;
}

TEST(Bud, terminal_bud_grows_axial_bud) {
  bool bud_is_terminal = true;
  Bud *bud = new Bud(/*direction=*/glm::vec3(1, 0, 0),
                     /*coaxis=*/glm::vec3(0, 0, 1), bud_is_terminal);
  std::pair<Node *, Bud *> shoot_and_bud = bud->grow_shoot_and_bud(
      /*position=*/glm::vec3(0, 10, 0),
      /*light_gradient=*/glm::vec3(0, 0, -1), BranchGrowth::basic());

  EXPECT_NE(shoot_and_bud.second, nullptr);
  EXPECT_TRUE(shoot_and_bud.second->is_axial());
  delete shoot_and_bud.first;
  delete shoot_and_bud.second;
}

TEST(Bud, axial_bud_grows_no_bud) {
  bool bud_is_terminal = false;
  Bud *bud = new Bud(/*direction=*/glm::vec3(1, 0, 0),
                     /*coaxis=*/glm::vec3(0, 0, 1), bud_is_terminal);
  std::pair<Node *, Bud *> shoot_and_bud = bud->grow_shoot_and_bud(
      /*position=*/glm::vec3(0, 10, 0),
      /*light_gradient=*/glm::vec3(0, 0, -1), BranchGrowth::basic());

  EXPECT_EQ(shoot_and_bud.second, nullptr);
  delete shoot_and_bud.first;
  // Note that in this case we know the bud should be a nullptr, but in general
  // we might not know.
  delete shoot_and_bud.second;
}

TEST(Bud, grown_terminal_shoot_retains_bud) {
  bool bud_is_terminal = true;
  Bud *bud = new Bud(/*direction=*/glm::vec3(1, 0, 0),
                     /*coaxis=*/glm::vec3(0, 0, 1), bud_is_terminal);
  std::pair<Node *, Bud *> shoot_and_bud = bud->grow_shoot_and_bud(
      /*position=*/glm::vec3(0, 10, 0),
      /*light_gradient=*/glm::vec3(0, 0, -1), BranchGrowth::basic());

  const Node &const_shoot = *shoot_and_bud.first;
  EXPECT_EQ(bud, const_shoot.get_bud());
  delete shoot_and_bud.first;
  delete shoot_and_bud.second;
}

TEST(Bud, grown_axial_shoot_retains_bud) {
  bool bud_is_terminal = false;
  Bud *bud = new Bud(/*direction=*/glm::vec3(1, 0, 0),
                     /*coaxis=*/glm::vec3(0, 0, 1), bud_is_terminal);
  std::pair<Node *, Bud *> shoot_and_bud = bud->grow_shoot_and_bud(
      /*position=*/glm::vec3(0, 10, 0),
      /*light_gradient=*/glm::vec3(0, 0, -1), BranchGrowth::basic());

  const Node &const_shoot = *shoot_and_bud.first;
  EXPECT_EQ(bud, const_shoot.get_bud());
  delete shoot_and_bud.first;
  delete shoot_and_bud.second;
}

TEST(Bud, grown_shoot_is_in_correct_position) {
  const glm::vec3 bud_direction(1, 0, 0);
  const glm::vec3 bud_position(0, 10, 0);
  const float metamer_length = BranchGrowth::basic().metamer_length();
  Bud *bud = new Bud(bud_direction, /*coaxis=*/glm::vec3(0, 0, 1),
                     /*is_terminal=*/true);

  std::pair<Node *, Bud *> shoot_and_bud = bud->grow_shoot_and_bud(
      bud_position, /*light_gradient=*/glm::vec3(0, 0, -1),
      BranchGrowth::basic());

  // Expects that after reorienting, the bud grew in a direction roughly similar
  // to bud_direction.
  EXPECT_THAT(shoot_and_bud.first->get_position().x,
              testing::FloatNear(metamer_length, metamer_length / 2));
  delete shoot_and_bud.first;
  delete shoot_and_bud.second;
}

TEST(Bud, bud_has_correct_direction_after_growth) {
  glm::vec3 direction(1, 0, 0);
  glm::vec3 coaxis(0, 0, 1);
  glm::vec3 up(0, 0, 1);
  Bud *bud = new Bud(direction, coaxis, /*is_terminal=*/true);

  glm::vec3 light_gradient(0, 1, 0);
  BranchGrowth completely_phototropic =
      BranchGrowth(Phyllotaxis::basic(), /*phototropism=*/1, /*gravitropism=*/0,
                   /*metamer_length=*/2);
  std::pair<Node *, Bud *> shoot_and_bud = bud->grow_shoot_and_bud(
      /*position=*/glm::vec3(0), light_gradient, completely_phototropic);

  EXPECT_THAT(bud->get_direction(),
              VectorIsNear(glm::normalize(light_gradient)));
  delete shoot_and_bud.first;
  delete shoot_and_bud.second;
}

TEST(Bud, bud_rotates_during_growth) {
  glm::vec3 bud_direction = glm::vec3(1, 0, 0);
  glm::vec3 starting_coaxis = glm::vec3(0, 0, 1);
  double rotation_angle = M_PI / 2;
  glm::vec3 expected_ending_coaxis = glm::vec3(0, -1, 0);
  Bud *bud = new Bud(bud_direction, starting_coaxis, /*is_terminal=*/true);

  std::pair<Node *, Bud *> shoot_and_bud = bud->grow_shoot_and_bud(
      /*position=*/glm::vec3(0, 10, 0),
      /*light_gradient=*/glm::vec3(0, 0, -1),
      BranchGrowth(Phyllotaxis(rotation_angle, /*branch_angle=*/M_PI / 3,
                               /*rotate_to_horizontal=*/0),
                   /*phototropism=*/.2, /*gravitropism=*/.2,
                   /*metamer_length=*/.5));

  EXPECT_THAT(bud->get_rotation_coaxis(), VectorIsNear(expected_ending_coaxis));
  delete shoot_and_bud.first;
  delete shoot_and_bud.second;
}

TEST(Bud, grown_axial_buds_have_correct_direction) {
  glm::vec3 bud_direction = glm::vec3(1, 0, 0);
  glm::vec3 bud_coaxis = glm::vec3(0, 0, 1);
  double branching_angle = M_PI / 2;
  glm::vec3 expected_axial_bud_direction = bud_coaxis;
  Bud *bud = new Bud(bud_direction, bud_coaxis, /*is_terminal=*/true);

  std::pair<Node *, Bud *> shoot_and_bud = bud->grow_shoot_and_bud(
      /*position=*/glm::vec3(0, 10, 0),
      /*light_gradient=*/glm::vec3(0, 0, -1),
      BranchGrowth(Phyllotaxis(/*rotation_angle=*/M_PI / 4, branching_angle,
                               /*rotate_to_horizontal=*/0),
                   /*phototropism=*/0, /*gravitropism=*/0,
                   /*metamer_length=*/.75));

  EXPECT_THAT(shoot_and_bud.second->get_direction(),
              VectorIsNear(expected_axial_bud_direction));
  delete shoot_and_bud.first;
  delete shoot_and_bud.second;
}

TEST(Bud, grown_shoot_has_correct_direction) {
  glm::vec3 direction(1, 0, 0);
  glm::vec3 coaxis(0, 0, 1);
  glm::vec3 up(0, 0, 1);
  Bud *bud1 = new Bud(direction, coaxis, /*is_terminal=*/true);
  Bud *bud2 = new Bud(*bud1);
  Bud *bud3 = new Bud(*bud1);
  Bud *bud4 = new Bud(*bud1);

  glm::vec3 bud_position(10, 0, 0);
  glm::vec3 light_gradient(0, 1, 0);

  double phototropism = .3;
  double gravitropism = .1;
  double metamer_length = 1;
  BranchGrowth ordinary_growth = BranchGrowth(
      Phyllotaxis::basic(), phototropism, gravitropism, metamer_length);
  BranchGrowth completely_phototropic =
      BranchGrowth(Phyllotaxis::basic(), /*phototropism=*/1, /*gravitropism=*/0,
                   metamer_length);
  BranchGrowth completely_gravitropic =
      BranchGrowth(Phyllotaxis::basic(), /*phototropism=*/0, /*gravitropism=*/1,
                   metamer_length);
  BranchGrowth completely_straight =
      BranchGrowth(Phyllotaxis::basic(), /*phototropism=*/0, /*gravitropism=*/0,
                   metamer_length);
  std::pair<Node *, Bud *> ordinary_shoot_and_bud =
      bud1->grow_shoot_and_bud(bud_position, light_gradient, ordinary_growth);
  std::pair<Node *, Bud *> phototropic_shoot_and_bud = bud2->grow_shoot_and_bud(
      bud_position, light_gradient, completely_phototropic);
  std::pair<Node *, Bud *> gravitropic_shoot_and_bud = bud3->grow_shoot_and_bud(
      bud_position, light_gradient, completely_gravitropic);
  std::pair<Node *, Bud *> straight_shoot_and_bud = bud4->grow_shoot_and_bud(
      bud_position, light_gradient, completely_straight);

  glm::vec3 expected_ordinary_direction =
      glm::normalize(
          trilerp(direction, light_gradient, up, phototropism, gravitropism)) *
      (float)metamer_length;
  EXPECT_THAT(ordinary_shoot_and_bud.first->get_direction(),
              VectorIsNear(expected_ordinary_direction));
  EXPECT_THAT(phototropic_shoot_and_bud.first->get_direction(),
              VectorIsNear(light_gradient));
  EXPECT_THAT(gravitropic_shoot_and_bud.first->get_direction(),
              VectorIsNear(up));
  EXPECT_THAT(straight_shoot_and_bud.first->get_direction(),
              VectorIsNear(direction));

  delete ordinary_shoot_and_bud.first;
  delete ordinary_shoot_and_bud.second;

  delete phototropic_shoot_and_bud.first;
  delete phototropic_shoot_and_bud.second;

  delete gravitropic_shoot_and_bud.first;
  delete gravitropic_shoot_and_bud.second;

  delete straight_shoot_and_bud.first;
  delete straight_shoot_and_bud.second;
}
