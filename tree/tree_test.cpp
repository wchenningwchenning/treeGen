#include "tree.hpp"

#include <gmock/gmock.h>

using testing::FloatNear;

BrightnessGetter uniform_brightness() {
  return [](glm::vec3) { return 1; };
}

GradientGetter up_gradient() {
  return [](glm::vec3) { return glm::vec3(0, 0, 1); };
}

BudPlacer no_bud() {
  return [](glm::vec3, double) { return; };
}

TEST(Tree, terminal_buds_grow_axial_ones) {
  Tree tree(Species(), uniform_brightness(), up_gradient(), no_bud());
  tree.tick_years(1);

  const Node &base = *tree.get_base_node();
  EXPECT_TRUE(base.has_axial_bud());
  EXPECT_EQ(base.get_branch(), nullptr);
  ASSERT_NE(base.get_trunk(), nullptr);
}

TEST(Tree, grown_shoots_are_correct_direction) {
  const double metamer_length = Species().branch_growth.metamer_length();
  Tree tree(Species(), uniform_brightness(), up_gradient(), no_bud());
  tree.tick_years(5);

  const Node &base = *tree.get_base_node();

  // Expects that the trunk is growing vertically at the base.
  EXPECT_THAT(base.get_trunk()->get_position().z,
              FloatNear(base.get_position().z +
                            Species().branch_growth.metamer_length(),
                        metamer_length * .05));

  // Expects that the tree's first branch has a slight curve upwards.
  EXPECT_GT(base.get_branch()->get_trunk()->get_position().z,
            base.get_branch()->get_position().z + metamer_length * .05);
}

TEST(Tree, grow_branch_bias) {
  const double mid_branch_bias = .5;
  const double low_branch_bias = .0;

  Tree trunk_biased(
      Species(ApicalControl(low_branch_bias), BranchGrowth::basic(), 100),
      uniform_brightness(), up_gradient(), no_bud());
  trunk_biased.tick_years(10);

  Tree unbiased(
      Species(ApicalControl(mid_branch_bias), BranchGrowth::basic(), 100),
      uniform_brightness(), up_gradient(), no_bud());
  unbiased.tick_years(10);

  // Expect extreme changes in branch bias to cause large change in max depth of
  // trees.
  EXPECT_GT(trunk_biased.get_base_node()->max_depth(),
            2 * unbiased.get_base_node()->max_depth());
}
