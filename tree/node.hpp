#ifndef NODE_HPP
#define NODE_HPP

// Needs to support
//   Calculate bud exposure
//   Update bud fates
//   Add shoots
//   Shed branches
//   Update branch width

#include <functional>
#include <glm/vec3.hpp>

class Bud;
// Holds shape of a tree
class Node {
  glm::vec3 direction_;
  glm::vec3 position_; // Could be calculated on the fly to save memory
  Node *next_node = nullptr;
  Node *branched_node = nullptr;
  Bud *bud = nullptr; // Could be a terminal or axial bud
  int num_shed_branches = 0;
  double exposure = 0; // Caches exposures calculated in other classes

public:
  void assert_invariants() const;

  bool has_bud() const { return bud != nullptr; };

  // Asserts that this node has a bud.
  bool bud_is_terminal() const;
  // Asserts that this node has a bud.
  bool bud_is_axial() const;

  bool has_terminal_bud() const { return has_bud() && bud_is_terminal(); }
  bool has_axial_bud() const { return has_bud() && bud_is_axial(); }
  bool has_outgoing_node() const { return next_node || branched_node; }

  size_t node_count() const;
  size_t bud_count() const;
  size_t max_depth() const;

  Node() {}
  Node(glm::vec3 direction, glm::vec3 position, Bud *bud)
      : direction_(direction), position_(position), bud(bud){};

  ~Node();

  glm::vec3 get_direction() const { return direction_; }
  glm::vec3 get_position() const { return position_; }

  int get_num_shed_branches() const { return num_shed_branches; }

  double get_exposure() const { return exposure; }
  void set_exposure(double new_exposure) { exposure = new_exposure; }

  Bud *get_bud() { return bud; }
  Bud const *get_bud() const { return bud; }
  void set_bud(Bud *new_bud) { bud = new_bud; };

  Node *get_branch() { return branched_node; }
  Node const *get_branch() const { return branched_node; }
  void set_branch(Node *new_branch) { branched_node = new_branch; };

  Node *get_trunk() { return next_node; }
  Node const *get_trunk() const { return next_node; }
  void set_trunk(Node *new_trunk) { next_node = new_trunk; };

  static Node *new_tree(glm::vec3 position);
};

#endif /* NODE_HPP */
