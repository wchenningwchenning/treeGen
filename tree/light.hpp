#ifndef LIGHT_HPP
#define LIGHT_HPP

#include <glm/vec3.hpp>
#include <vector>

// Stores the brightness of the volume around a tree. Used by growing trees to
// grow shoots in brighter directions.
class Light {
  const int x_size;
  const int y_size;
  const int z_size;
  const double voxel_length;
  const double kBrightnessError = .01;
  // Ranges from 1 to 0. Could be optimized by tracking shadow strength instead
  // and using calloc.
  std::vector<float> brightness;

  // Returns the index into shadow for a given voxel's coordinates.
  size_t index(int x, int y, int z) const;
  // Sets the x, y, z output args to the voxel coordinate of the given position.
  // Does not do any bounds checking.
  void coordinates_of_position(int *out_x, int *out_y, int *out_z,
                               glm::vec3 position) const;

  // Checks whether a given voxel coordinate is inside the shadow field.
  bool check_in_bounds(int x, int y, int z) const;

  // Asserts that a given voxel coordinate is inside the shadow field.
  void assert_in_bounds(int x, int y, int z) const;

  // Asserts that x,y,z coords are inside shadow field's bounds.
  void set_brightness(int x, int y, int z, double val);
  // Asserts that x,y,z coords are inside shadow field's bounds.
  void increase_brightness(int x, int y, int z, double val);
  // Asserts that x,y,z coords are inside shadow field's bounds.
  double get_brightness(int x, int y, int z) const;

  // Updates the brightness volume to account for the added shade of this bud.
  void add_bud(int x, int y, int z, double bud_size);

  // Caluculates the shadow volume casted by adding a new bud. Takes an
  // argument, bud_size, for the size of the bud casting the shadow. Then
  // calculates the shadow intensity in a voxel falloff as brightness =
  // k*cos(2t)/d^2 where t is the anngle the voxel is from the bud measured from
  // directly downwards. This function is somewhat physically derived with
  // shadow intensity falling of with the square of distance. However, the use
  // of cos(2t) is an engineering decision to ensure the shadow is zero outside
  // of a cone below the bud to prevent excessive writes to the shadow volume
  // for each bud added.
  //
  // Has no requirement on the bounds of the given voxel coordinates.
  double shadow_intensity(int bud_x, int bud_y, int bud_z, int voxel_x,
                          int voxel_y, int voxel_z, double bud_size) const;

public:
  // Checks whether a given point is inside the shadow field.
  bool check_in_bounds(glm::vec3 position) const;

  // Creates a light volume with dimensions [-length/2, length/2] x [-length/2,
  // length/2] x [0, length]. Requires that resolution is divisible by two to
  // ensure the grid centers around the tree's origin.
  Light(int resolution, double length)
      : Light(resolution, resolution, resolution, length / resolution){};

  // Creates a light volume with dimensions [-x_resolution/2*voxel_length,
  // x_resolution/2*voxel_length] x [-y_resolution/2*voxel_length,
  // y_resolution/2*voxel_length] x [0, z_resolution*voxel_length]. Requires
  // that resolution is divisible by two to ensure the grid centers around the
  // tree's origin.
  Light(int x_resolution, int y_resolution, int z_resolution,
        double voxel_length);

  // Decreases the brightness volume to according to the shadow casted by
  // the given bud. Any of the bud's shadow extending beyond the brightness
  // volume's bounds is ignored;
  void add_bud(glm::vec3 position, double bud_size);

  // Returns unshadowed brightness if given position is outside of shadow
  // field's bounds.
  double get_brightness(glm::vec3 position) const;

  // Returns the gradient of the brightness field at this point. If the
  // given point is outside the brightness field, a zero vector is returned.
  glm::vec3 get_gradient(glm::vec3 position) const;
};

#endif /* LIGHT_HPP */
