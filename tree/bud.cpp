#include "tree/bud.hpp"
#include "math/vector_math.hpp"
#include "tree/node.hpp"

#include <glm/glm.hpp>
#include <utility>

#include <iostream>
void Bud::reorient(glm::vec3 light_gradient,
                   const BranchGrowth &branch_growth) {
  glm::vec3 up = glm::vec3(0, 0, 1);

  glm::vec3 light_dir = direction;
  if (glm::length(light_gradient) != 0) {
    light_dir = glm::normalize(light_gradient);
    // light_dir = light_gradient;
  }

  glm::vec3 next_direction =
      trilerp(direction, light_dir, up, branch_growth.phototropism(),
              branch_growth.gravitropism());

  // If the new direction is identically zero, don't reorient this bud.
  if (next_direction != glm::vec3(0)) {
    direction = glm::normalize(next_direction);
  }
  rotation_coaxis = rotate_to_perpendicular(rotation_coaxis, direction);
}

void Bud::rotate(const BranchGrowth &branch_growth) {
  set_rotation_coaxis(branch_growth.get_phyllotaxis().rotate_coaxis(
      direction, rotation_coaxis));
}

std::pair<Node *, Bud *>
Bud::grow_shoot_and_bud(glm::vec3 bud_position, glm::vec3 light_gradient,
                        const BranchGrowth &branch_growth) {
  Bud *grown_bud = nullptr;
  if (is_terminal()) {
    glm::vec3 axial_direction =
        branch_growth.get_phyllotaxis().branch_direction(direction,
                                                         rotation_coaxis);
    glm::vec3 axial_coaxis = branch_growth.get_phyllotaxis().branch_coaxis(
        direction, axial_direction);
    grown_bud = new Bud(axial_direction, axial_coaxis, /*terminal=*/false);
  }

  reorient(light_gradient, branch_growth);
  rotate(branch_growth);

  glm::vec3 displacement = direction * (float)branch_growth.metamer_length();
  Node *shoot = new Node(displacement, bud_position + displacement, this);

  terminal = true;
  return {shoot, grown_bud};
}
